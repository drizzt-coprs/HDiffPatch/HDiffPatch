%global libmd5_commit 51edeb63ec3f456f4950922c5011c326a062fbce
%global lzma_commit   c80bb80bbbac61da7ec02d12fb497851b1f59635

Name:           HDiffPatch
Version:        3.0.8
Release:        1%{?dist}
Summary:        a C/C++ library and command-line tools for binary data Diff & Patch

License:        MIT
URL:            https://github.com/sisong/HDiffPatch
Source0:        https://github.com/sisong/HDiffPatch/archive/v%{version}.tar.gz#/hdiffpatch-%{version}.tar.gz
Source1:        https://github.com/sisong/libmd5/archive/%{libmd5_commit}.tar.gz#/libmd5-%{libmd5_commit}.tar.gz
Source2:        https://github.com/sisong/lzma/archive/%{lzma_commit}.tar.gz#/lzma-%{lzma_commit}.tar.gz

BuildRequires:  gcc-c++
BuildRequires:  bzip2-devel
BuildRequires:  zlib-devel
#Requires:       

%description
a C/C++ library and command-line tools for binary data Diff & Patch
fast and create small delta/differential
support large files and directory(folder) and limit memory requires both diff & patch.


%prep
%autosetup -p1
%setup -q -D -T -a 1 -a 2
mv libmd5-%{libmd5_commit} libmd5
mv lzma-%{lzma_commit} lzma
sed -i -e 's:\.\./libmd5:./libmd5:g' -e 's:\.\./lzma:./lzma:g' Makefile


%build
CFLAGS="$RPM_OPT_FLAGS $RPM_LD_FLAGS" CXXFLAGS="$RPM_OPT_FLAGS $RPM_LD_FLAGS" %make_build -j1 MT=1 MD5=1


%install
rm -rf $RPM_BUILD_ROOT
install -dm755 $RPM_BUILD_ROOT/%{_bindir}
%make_install INSTALL_BIN='$(DESTDIR)/%{_bindir}'


%files
%license LICENSE
%doc README.md
%{_bindir}/hdiffz
%{_bindir}/hpatchz


%changelog
* Sat Jun 27 2020 Timothy Redaelli <tredaelli@redhat.com> - 3.0.8-1
- New package

